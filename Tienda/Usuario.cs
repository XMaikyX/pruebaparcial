﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tienda
{
    class Usuario 
    {
       
        private List<Cliente> clientes;

        public List<Cliente> Clientes
        {
            get { return clientes; }
            set { clientes = value; }
        }
        public void AgregarCliente (Cliente Nuevocliente)
        {
            this.Clientes.Add(Nuevocliente);
        }
        public void RetirarCliente(Cliente Retiradocliente)
        {
            this.Clientes.Remove(Retiradocliente);
        }
    }
}
